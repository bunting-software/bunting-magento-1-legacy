# Installing Bunting on Magento 1

To download the plugin click on Downloads in the left hand menu then click the `Download Repository` link.

Once you have the Bunting plugin `bunting-magento-1-legacy` folder you should see the following folders:
`app/` `js/` `skin/`.

To manually install all you have to do is add these folders/directories into your sites Magento root installation folder.

Once the files have been added you will need to clear your Magento sites cache by going to `System -> Cache Management` 
select all and set the action to refresh then click submit.

>If you are using Magerun you can use `magerun cache:flush`.

If the previous steps have been successful for whichever method you follow you should now see `Bunting` on the top of 
the navigation bar. Navigate here and sign in to your Bunting account to complete the installation process:
Enter your existing Bunting account details or if you are new to Bunting and want to get started, click the `CREATE ACCOUNT` button.