window.buntingLoaded = true;

jQuery( document ).ready(function( $ ) {
    $('#billing-freemium, #billing-automatic').addClass('btn btn-primary');
    $('#choosePlanButton').click(function() {
        $('#loginForm').fadeOut(function() {
            $('#registerForm').fadeIn();
        });

    });
    $('#registerForm .back').click(function() {
        $('#registerForm').fadeOut(function() {
            $('#loginForm').fadeIn();
        });
    });
    jQuery.validator.addMethod("subdomain", function(value, element) {
        return this.optional( element ) || /^[a-z0-9\-_]+$/.test( value );
    }, "Please specify a valid subdomain, using lowercase letters, numbers, hyphens and underscores.");
    $("#actualLoginForm").validate({
        rules: {
            verify_bunting_subdomain: {
                required: true,
                maxlength: 50
            },
            verify_email_address: {
                required: true,
                email: true
            },
            verify_password: {
                required: true,
                maxlength: 100
            }
        },
        submitHandler: function(form) {
            submitForm(form, 'login', 'verify');
        }
    });
    $("#actualRegisterForm").validate({
        rules: {
            register_email_address: {
                required: true,
                email: true
            },
            register_password: {
                required: true,
                minlength: 8,
                maxlength: 100
            },
            password_confirmation: {
                required: true,
                equalTo: "#registerForm #register_password"
            },
            forename: {
                required: true,
                maxlength: 100
            },
            surname: {
                required: true,
                maxlength: 100
            },
            company_name: {
                required: true,
                maxlength: 100
            },
            register_subdomain: {
                required: true,
                subdomain: true,
                maxlength: 100
            },
            promotional_code: {
                maxlength: 20
            }
        },
        submitHandler: function(form) {
            submitForm(form, 'register', 'register');
        }
    });

    window.allowPasswordTrigger = false;
    $('.forgotPasswordTrigger').click(function(e){
        if (window.allowPasswordTrigger) {
            return true;
        }

        e.preventDefault();
        e.stopPropagation();
        $('.forgotPasswordForm').toggleClass('active');
        return false;
    });

    $('.forgotPasswordForm .btn').click(function(e){
        e.preventDefault();
        e.stopPropagation();
        var subdomain = $('.forgotPasswordForm #bunting_forgot_subdomain').val(),
            url = 'https://' + subdomain + '.1.bunting.com/login?a=lost_password';

        jQuery.get('/index.php/bunting/index/exists/subdomain/' + subdomain, function(data){
            if (parseInt(data) === 1) {
                window.location = url;
            } else {
                alert('The subdomain you entered is incorrect');
            }
        });
    });
});
function submitForm(form, type, prefix) {
    var $this = jQuery(form),
        values = serializeFormValues($this),
        fieldErrorMappings = {
            subdomain: prefix + '_bunting_subdomain',
            email_address: prefix + '_email_address',
            property: prefix + '_password',
            name: 'company_name',
            confirm_password: 'password_confirmation',
            validation: 'verify_password'
        };

    jQuery('#loading').show();
    jQuery('label.error').hide();
    jQuery('input.error').removeClass('error');

    jQuery.ajax({
        type: "POST",
        url: type === "login" ? window.buntingLoginUrl : window.buntingRegisterUrl,
        data: values,
        dataType: 'json',
        success: function(data) {
            if (typeof data.errors === 'undefined') {
                window.location.href = jQuery('#success-url').val();
            }
            else {
                for (var property in data.errors) {
                    if (data.errors.hasOwnProperty(property)) {
                        var value = data.errors[property];
                        console.log(property);
                        property = fieldErrorMappings.hasOwnProperty(property) ? fieldErrorMappings[property] : property;

                        if (property === 'validation') {
                            jQuery('#verify_email_address').addClass('error');
                        }

                        var element = jQuery('#'+property);

                        if (element.length) {
                            element.addClass('error').parent().after('<label class="error">' + value + '</label>');
                        } else {
                            jQuery('.login button.btn-last, #actualRegisterForm button.btn-info').before('<label class="error">' + value + '</label>');
                        }
                    }
                }
            }
            jQuery('#loading').hide();
        },
        always: function() {
            jQuery('#loading').hide();
        }
    });
}

function serializeFormValues(form) {
    return form.serializeArray().reduce(function(obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});
}