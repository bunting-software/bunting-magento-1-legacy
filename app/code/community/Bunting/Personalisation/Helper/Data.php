<?php

/**
 * Class Bunting_Personalisation_Helper_Data
 */
class Bunting_Personalisation_Helper_Data extends Mage_Core_Helper_Abstract {
    /**
     * @return integer
     */
    public function getTopLevelProductId() {
        $product = Mage::registry('current_product');

        if (!$product->isConfigurable()) {
            $parentIds = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($product->getId());
            $productId = isset($parentIds[0]) ? $parentIds[0] : null;
        }

        return !isset($productId) || is_null($productId) ? $product->getId() : $productId;
    }

    /**
     * Offers aren't supported on bundled products outside of the core Magento functionality
     *
     * @return bool
     */
    public function isProductInOffer() {
        $product = Mage::registry('current_product');
        return $product->getTypeId() === Mage_Catalog_Model_Product_Type::TYPE_BUNDLE ? false : !in_array($product->getSpecialPrice(), array(false, null), true);
    }

    /**
     * @return float
     */
    public function getProductDisplayPrice() {
        $product = Mage::registry('current_product');

        if ($product->getTypeId() !== Mage_Catalog_Model_Product_Type::TYPE_BUNDLE) {
            return number_format($product->getSpecialPrice() ? $product->getSpecialPrice() : $product->getPrice(), 2);
        }
        $block = Mage::getSingleton('core/layout')->createBlock('bundle/catalog_product_view_type_bundle');
        $options = $block->setProduct($product)->getOptions();
        $price = 0;

        foreach ($options as $option) {
            $selections = $option->getSelections();
            $lowestSelectionPrice = pow(10, 10);

            foreach ($selections as $selection) {
                $lowestSelectionPrice = $selection->getFinalPrice() < $lowestSelectionPrice
                    ? Mage::helper('tax')->getPrice($selection, $selection->getFinalPrice())
                    : $lowestSelectionPrice;
            }

            $price += $lowestSelectionPrice != pow(10, 10) ? $lowestSelectionPrice : 0;
        }

        return number_format($price, 2);
    }

    /**
     * @return float
     */
    public function getProductSaving() {
        $product = Mage::registry('current_product');
        return number_format($product->getPrice() - $this->getProductDisplayPrice(), 2);
    }

    /**
     * @return integer
     */
    public function getProductSpecialPriceEndDate() {
        $product = Mage::registry('current_product');
        return (new DateTime($product->getSpecialToDate()))->getTimestamp();
    }

    /**
     * @return string|null
     */
    public function getProductBrand() {
        $product = Mage::registry('current_product');
        return $product->getData('manufacturer') ? $product->getAttributeText('manufacturer') : null;
    }

    /**
     * @return string|false
     */
    public function getProductCategoryBreadcrumb() {
        $cats = Mage::registry('current_product')->getCategoryIds();
        return isset($cats[0]) ? $this->getCategoryBreadcrumb(Mage::getModel('catalog/category')->load($cats[0])) : false;
    }

    /**
     * @param $category
     * @param string $breadcrumb
     * @return bool|string
     */
    public function getCategoryBreadcrumb($category, $breadcrumb = '') {
        if (!$category) {
            return false;
        }

        $parentCategory = $category->getParentCategory();
        $breadcrumb = $category->getName() . (strlen($breadcrumb) ? "&gt;" : '') . $breadcrumb;
        return !is_null($parentCategory->getName()) ? $this->getCategoryBreadcrumb($parentCategory, $breadcrumb) : $breadcrumb;
    }

    /**
     * @param $url
     * @return string
     */
    public function getAbsoluteImageUrl($url) {
        return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'catalog/product' . $url;
    }

    /**
     * Bunting refers to languages by a 3 digit code (for example, ENG) instead of a standard localisation code (for example, en_GB)
     * This method maps those
     *
     * @return string|null
     */
    public function getBuntingStoreLanguageCode() {
        $code = strtoupper(substr(Mage::getStoreConfig('general/locale/code', Mage::app()->getStore()->getId()), 0, 2));
        $mapping = $this->getBuntingLanguageCodeMapping();
        return isset($mapping[$code]) ? $mapping[$code] : 'UNK';
    }

    /**
     * @return array
     */
    private function getBuntingLanguageCodeMapping() {
        return array(
            'EN' => 'ENG',
            'FR' => 'FRA',
            'DE' => 'DEU',
            'AR' => 'ARA',
            'ES' => 'SPA',
            'BN' => 'BEN',
            'ZH' => 'ZHO',
            'HI' => 'HIN',
            'RU' => 'RUS',
            'PT' => 'POR',
            'JA' => 'JPN',
            'JV' => 'JAV',
            'KO' => 'KOR',
            'TR' => 'TUR',
            'VI' => 'VIE',
            'TE' => 'TEL',
            'MR' => 'MAR',
            'TA' => 'TAM',
            'IT' => 'ITA',
            'UR' => 'URD',
            'GU' => 'GUJ',
            'PL' => 'POL',
            'UK' => 'UKR',
            'NL' => 'NLD',
            'CS' => 'CES',
            'DA' => 'DAN',
            'SK' => 'SLO',
            'HE' => 'HEB'
        );
    }
}