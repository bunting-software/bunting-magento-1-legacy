<?php
class Bunting_Personalisation_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * @var string
     */
    private $xmlOutput;

    /**
     * Constructor
     *
     * @param Zend_Controller_Request_Abstract $request
     * @param Zend_Controller_Response_Abstract $response
     * @param array $invokeArgs
     */
    public function __construct(Zend_Controller_Request_Abstract $request, Zend_Controller_Response_Abstract $response, array $invokeArgs = array())
    {
        parent::__construct($request, $response, $invokeArgs);
        $this->xmlOutput = '';
    }

    /**
     * Generate and output a Bunting compliant product feed
     *
     * @return $this|Zend_Controller_Response_Abstract
     */
    public function indexAction()
    {
        $storeId = Mage::app()->getStore()->getStoreId();
        $buntingCollection = Mage::getModel('bunting_personalisation/bunting')->getCollection();
        $bunting = null;

        if (!$buntingCollection->count()) {
            return $this->sendError(501, "This application has no website_monitors attached. Please logout and login again.");
        }

        foreach ($buntingCollection as $buntingObject) {
            $bunting = $buntingObject['store_id'] == $storeId ? $buntingObject : $bunting;
        }

        if (is_null($bunting)) {
            return $this->sendError(500, "No store IDs match.");
        }

        // What Bunting receives
        $feedToken = $this->getRequest()->getParam('feed_token');
        $limit = is_numeric($this->getRequest()->getParam('size')) ? min((int) $this->getRequest()->getParam('size'), 20000) : 1;
        $page = is_numeric($this->getRequest()->getParam('page')) ? ((int) $this->getRequest()->getParam('page')) : 0;
        $lastPage = floor($this->getEnabledProductCollection()->getSize() / $limit);
        $addToBasketEnabled = $this->getRequest()->getParam('add_to_basket_enabled');

        // Internal pagination to prevent OOM errors
        $internalLimit = min(200, $limit);
        $internalPage = $page * ceil($limit / $internalLimit) + 1;
        $internalLastPage = (1 + $page) * (ceil($limit / $internalLimit));

        // Some pre-caching to improve performance
        $gtinAttribute = $this->getGtinAttributeCode();
        $store = $this->getStoreData();
        $bundleModel = Mage::getModel('bundle/product_price');
        $configurableProductModel = Mage::getModel('catalog/product_type_configurable');
        $imageStyle = Mage::getStoreConfig('bunting/settings/image_style', 0);
        $hideLowStock = Mage::getStoreConfig('bunting/settings/hide_low_stock', 0);
        $hideOutOfStock = Mage::getStoreConfig('bunting/settings/hide_out_of_stock', 0);

        if (!$feedToken || !$bunting || !$bunting->getFeedToken() || $feedToken !== $bunting->getFeedToken()) {
            return $this->sendError(400, 'Bad Request');
        }

        if (is_null($store)) {
            return $this->sendError(500, 'Issue loading store product data');
        }

        $this->createXmlMetaOpen($bunting->getBuntingSubdomain(), $bunting->getBuntingWebsiteMonitorId(), $bunting->getServerRegionSubdomainId(), $page < $lastPage ? 'no' : 'yes');

        while ($internalPage <= $internalLastPage) {
            foreach ($this->getEnabledProducts($internalLimit, $internalPage) as $product) {
                /** @var Mage_Catalog_Model_Product $product */
                $cheapestProduct = $product;

                if ($product->isConfigurable()) {
                    try {
                        $cheapestProduct = $this->getCheapestProductFromConfigurable($product);
                    } catch (\Exception $e) {
                        continue;
                    }

                    if (is_null($cheapestProduct)) {
                        continue;
                    }
                }

                $isBundle = $product->getTypeId() === Mage_Catalog_Model_Product_Type::TYPE_BUNDLE;
                $cats = $product->getCategoryIds();
                $categoryString = isset($cats[0]) ? Mage::helper('bunting_personalisation')->getCategoryBreadcrumb(Mage::getModel('catalog/category')->load($cats[0])) : false;
                $price = !$isBundle ? Mage::helper('tax')->getPrice($cheapestProduct, $cheapestProduct->getPrice(), true) : $bundleModel->getTotalPrices($product, 'min', 1);
                $specialPrice = Mage::helper('tax')->getPrice($cheapestProduct, $cheapestProduct->getSpecialPrice(), true);
                $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
                $stockQty = $stockItem->getIsInStock()
                    ? $product->isConfigurable()
                        ? $this->getConfigurableStock($product, $hideLowStock, $hideOutOfStock)
                        : ($hideOutOfStock == '1' && (int) $stockItem->getQty() <= 5 ? 0 : (int) $stockItem->getQty())
                    : 'n';
                $parentIds = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($product->getId());
                $productIsChild = !$product->isConfigurable() && !empty($parentIds);
                $isMissingData = $product->getImage() == 'no_selection' || !$categoryString;

                if ($productIsChild || $isMissingData || $price == 0 || ($cheapestProduct->getSpecialToDate() && $specialPrice == 0)) {
                    continue;
                }

                $otherImages = $product->getMediaGalleryImages();
                $tags = $this->getStoreProductTags($storeId, $product->getId());
                $rating = Mage::getModel('review/review_summary')->setStoreId($storeId)->load($product->getId())['rating_summary'];

                $this->createXmlContainer('product', true, 1);

                $this->createXmlContainer('ns', true, 2);
                $this->createXmlTag($store['language'], $store['language'] && strlen($store['language']) ? $product->getName() : 'Unknown product name', 3);
                $this->createXmlContainer('ns', false, 2);

                $this->createXmlContainer('ps', true, 2);
                if (!is_null($store['currency'])) {
                    $priceConverted = number_format($specialPrice ? $specialPrice : $price, 2, '.', false);
                    $this->createXmlTag($store['currency']->getCode(), $priceConverted, 3);
                }
                $this->createXmlContainer('ps', false, 2);

                if ($specialPrice && $price != $specialPrice) {
                    $this->createXmlContainer('oss', true, 2);
                    if (!is_null($store['currency'])) {
                        $savingConverted = number_format($price - $specialPrice, 2, '.', false);
                        $this->createXmlTag($store['currency']->getCode(), $savingConverted, 3);
                    }
                    $this->createXmlContainer('oss', false, 2);
                    $this->createXmlTag('oe', strtotime($cheapestProduct->getSpecialToDate()), 2, (bool)$cheapestProduct->getSpecialToDate());
                }

                if ($otherImages && method_exists($otherImages, 'getItems') && count($otherImages->getItems()) > 1) {
                    $otherImages = $otherImages->getItems();
                    $keys = array_keys($otherImages);
                    $image = Mage::helper('catalog/image')->init($product, 'image', $otherImages[$keys[1]]->getFile());
                    $this->createXmlTag('i2u', !$imageStyle || $imageStyle === '0' ? $image->resize(600, 600) : $image->keepFrame(false)->resize(600, 600), 2);
                }

                $this->createXmlTag('upc', $product->getId(), 2);
                $this->createXmlTag('kw', $this->getTagNames($tags), 2, $tags->count() > 0);

                $stockValue = !is_numeric($stockQty)
                    ? $stockQty
                    : ($stockQty < 1 && $stockItem->getBackorders() !== Mage_CatalogInventory_Model_Stock::BACKORDERS_NO ? 'y' : $stockQty);
                $this->createXmlTag('s', $stockValue, 2);

                if ($product->getImage() != 'no_selection') {
                    $image =  Mage::helper('catalog/image')->init($product, 'image');
                    $this->createXmlTag('iu', !$imageStyle || $imageStyle === '0' ? $image->resize(600, 600) : $image->keepFrame(false)->resize(600, 600), 2);
                }

                if ($product->getResource()->getAttribute('manufacturer')) {
                    $this->createXmlTag('b', $product->getAttributeText('manufacturer'), 2, (bool)$product->getAttributeText('manufacturer'));
                }

                $this->createXmlTag('gtin', $product->{"get{$gtinAttribute}"}(), 2, ($gtinAttribute && $product->{"get{$gtinAttribute}"}));
                $this->createXmlTag('c', $categoryString, 2, (bool)$categoryString);
                $this->createXmlTag('rt', $rating / 20, 2, (bool)$rating);
                $this->createXmlTag('u', $product->getProductUrl(), 2);
                $this->createXmlTag('cv1', number_format($product->getMsrp(), 2), 2, $product->getMsrp() > 0);

                if ($product->getTypeId() === Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE && $addToBasketEnabled == "yes") {
                    $size = '';

                    foreach ($configurableProductModel->getUsedProducts(null, $product) as $child) {
                        if ($product->getResource()->getAttribute('size')) {
                            $size .= $child->getId() . ':' . $child->getAttributeText('size') . ',';
                        }
                    }

                    $this->createXmlTag('cv2', rtrim($size, ','), 2);
                }

                $this->createXmlContainer('product', false, 1);
            }

            $internalPage++;
        }

        $this->createXmlContainer('feed', false);
        $this->loadLayout(false);
        $this->getResponse()->clearHeaders()->setHeader("Content-Type", "text/xml", true)->setBody($this->xmlOutput);
        return $this;
    }

    /**
     * Checks whether a remove subdomain exists within Bunting
     * This is a bit of a hack and once Bunting supports a better mechanism, we should use that instead
     */
    public function existsAction() {
        $subdomain = $this->getRequest()->getParam('subdomain');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,'https://' . $subdomain . '.1.bunting.com/login?a=lost_password');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        $response = curl_exec($ch);

        if (curl_getinfo($ch, CURLINFO_HTTP_CODE) == 200) {
            echo 1;
            return;
        }

        echo 0;
    }

    /**
     * @param $tag
     * @param bool $open
     * @param int $indent   Number of \t to render
     */
    private function createXmlContainer($tag, $open = true, $indent = 0) {
        $this->xmlOutput .= str_repeat("\t", $indent) . ($open ? "<{$tag}>\n" :  "</{$tag}>\n");
    }

    /**
     * Append a valid XML tag with properly escaped CDATA to $this->xmlOutput
     *
     * @param string $tag
     * @param string $value
     * @param int $indent   Number of \t to render
     * @param bool $conditional    If you pass a false to this (IE, a false conditional) it won't render the tag.
     *                              this is useful for rendering if statements unnecessary when rendering tags
     */
    private function createXmlTag($tag, $value, $indent = 0, $conditional = true) {
        if ($conditional) {
            $this->xmlOutput .= str_repeat("\t", $indent) . "<{$tag}><![CDATA[{$value}]]></{$tag}>\n";
        }
    }

    /**
     * @param string $domain
     * @param int $feedId
     * @param int $regionId
     * @param string $lastPage
     *
     * @param int
     */
    private function createXmlMetaOpen($domain, $feedId, $regionId, $lastPage) {
        $this->xmlOutput .= '<?xml version="1.0" encoding="UTF-8" standalone="no"?>'."\n";
        $this->xmlOutput .= '<!DOCTYPE feed SYSTEM "https://' . $domain . (is_numeric($regionId) ? '.' . $regionId : '') . '.bunting.com/feed-' . $feedId . '.dtd">'."\n";
        $this->xmlOutput .= '<feed last_page="' . $lastPage . '">'."\n";
    }

    /**
     * @param $tags
     * @return string
     */
    private function getTagNames($tags) {
        if (!count($tags)) {
            return '';
        }

        $tagString = '';
        foreach ($tags as $tag) {
            $tagString .= $tag->getName() . ' ';
        }

        return $tagString;
    }

    /**
     * GTIN isn't a globally recognised value, so localise GTINs to get the best approximation
     *
     * @return bool|string
     */
    private function getGtinAttributeCode() {
        foreach (['ean', 'upc', 'jan', 'isbn'] as $code) {
            if ($this->attributeCodeExists($code)) {
                return ucfirst($code);
            }
        }

        return false;
    }

    /**
     * @param $storeId
     * @param $productId
     * @return mixed
     */
    private function getStoreProductTags($storeId, $productId) {
        $model = Mage::getModel('tag/tag');
        return $model->getResourceCollection()
            ->addPopularity()
            ->addStatusFilter($model->getApprovedStatus())
            ->addProductFilter($productId)
            ->setFlag('relation', true)
            ->addStoreFilter($storeId)
            ->setActiveFilter()
            ->load();
    }

    /**
     * @return mixed
     */
    private function getEnabledProductCollection() {
        return Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('status', array('eq' => 1))
            ->addAttributeToFilter('small_image', array('notnull'=>'','neq'=>'no_selection'));
    }

    /**
     * @param int $limit
     * @param int $page
     * @return mixed
     */
    private function getEnabledProducts($limit, $page) {
        $products = $this->getEnabledProductCollection()
            ->setOrder('date','ASC');

        $products->getSelect()->limitPage($page, $limit);
        $products->load();

        $this->addMediaGalleryToProductCollection($products);
        return $products;
    }

    /**
     * @param array $ids
     * @return mixed
     */
    private function getEnabledProductsByIds(array $ids) {
        return Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect(array('id', 'price', 'special_price', 'special_to_date'))
            ->addAttributeToFilter('status', array('eq' => 1))
            ->addFieldToFilter('entity_id', array('in'=> $ids))
            ->load();
    }

    /**
     * Returns the product with the lowest final price after special offers are considered, but which has a non-zero value
     *
     * @param Mage_Catalog_Model_Product $product
     * @return Mage_Catalog_Model_Product|null
     */
    public function getCheapestProductFromConfigurable(Mage_Catalog_Model_Product $product) {
        $cheapestProduct = $this->isProductFree($product) ? null : $product;
        $usedProductIds = Mage::getModel('catalog/product_type_configurable')->getChildrenIds($product->getId());

        if (isset($usedProductIds[0]) && is_array($usedProductIds[0]) && !empty($usedProductIds[0])) {
            /** @var Mage_Catalog_Model_Product_Collection $collection */
            $collection = Mage::getModel('catalog/product')->getCollection()
                ->addAttributeToSelect('price')
                ->addAttributeToSelect('special_price')
                ->addAttributeToSelect('special_to_date')
                ->addAttributeToFilter('entity_id', $usedProductIds);

            foreach ($collection as $child) {
                $cheapestProduct = !$this->isProductFree($child) && (is_null($cheapestProduct) || ($this->getLowestProductPrice($child) < $this->getLowestProductPrice($cheapestProduct)))
                    ? $child
                    : $cheapestProduct;
            }
        }

        return $cheapestProduct;
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return mixed
     */
    private function getLowestProductPrice(Mage_Catalog_Model_Product $product) {
        return $product->getSpecialPrice() ? $product->getSpecialPrice() : $product->getPrice();
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return bool
     */
    private function isProductFree(Mage_Catalog_Model_Product $product) {
        return ($product->getSpecialPrice() ? $product->getSpecialPrice() : $product->getPrice()) <= 0;
    }

    /**
     * @param $product
     * @param $hideLowStock
     * @param $hideOutOfStock
     * @return int
     */
    private function getConfigurableStock($product, $hideLowStock = 0, $hideOutOfStock = 0) {
        $stockQty = 0;
        $subIds = Mage::getResourceSingleton('catalog/product_type_configurable')
            ->getChildrenIds($product->getId());


        if (isset($subIds[0]) && is_array($subIds[0]) && !empty($subIds[0])) {
            $itemCount = 0;
            $inStockItemCount = 0;
            $subProducts = $this->getEnabledProductsByIds($subIds[0]);

            foreach($subProducts as $subProduct) {
                $subStockItem = Mage::getModel('cataloginventory/stock_item')
                    ->loadByProduct($subProduct->getId());

                $stockQty = $subStockItem->getIsInStock() ? (int) $subStockItem->getQty() + $stockQty : $stockQty;
                $inStockItemCount += $subStockItem->getIsInStock() ? 1 : 0;
                $itemCount++;
            }

            if ($hideLowStock == '1' && $itemCount > 0 && ($inStockItemCount === 0 || ($inStockItemCount / $itemCount) <= 0.5)) {
                return 0;
            }
        }

        return $hideOutOfStock == '1' && $stockQty <= 5 ? 0 : $stockQty;
    }

    /**
     * @return array
     */
    private function getStoreData() {
        $store = Mage::app()->getStore();
        $full_locale = Mage::getStoreConfig('general/locale/code', $store->getId());
        list($language, $locale) = explode('_', $full_locale);

        return [
            'store' => $store,
            'currency' => $store->getDefaultCurrency(),
            'language' => $language
        ];
    }

    /**
     * @param $code
     * @param $message
     * @return Zend_Controller_Response_Abstract
     */
    private function sendError($code, $message) {
        return $this->getResponse()
            ->clearHeaders()
            ->setHeader('HTTP/1.0', $code, true)
            ->setHeader('Content-Type', 'text/html')
            ->setBody("{$code} {$message}");
    }

    /**
     * @param string $code
     * @return bool
     */
    private function attributeCodeExists($code) {
        return (bool) Mage::getResourceModel('catalog/eav_attribute')->loadByCode('catalog_product', $code)->getId();
    }

    /**
     * This is a performance hack which adds all the product images to the products in a single query instead
     *
     * @param $productCollection
     */
    private function addMediaGalleryToProductCollection(&$productCollection)
    {
        $ids = array();
        foreach ($productCollection as $product) {
            $ids[] = $product->getEntityId();
        }

        $resource = Mage::getSingleton('core/resource');
        $conn = $resource->getConnection('catalog_read');
        $select = $conn->select()
            ->from(
                array('mg' => $resource->getTableName('catalog/product_attribute_media_gallery')),
                array(
                    'mg.entity_id', 'mg.attribute_id', 'mg.value_id', 'file' => 'mg.value',
                    'mgv.label', 'mgv.position', 'mgv.disabled',
                    'label_default' => 'mgdv.label',
                    'position_default' => 'mgdv.position',
                    'disabled_default' => 'mgdv.disabled'
                )
            )
            ->joinLeft(
                array('mgv' => $resource->getTableName('catalog/product_attribute_media_gallery_value')),
                '(mg.value_id=mgv.value_id AND mgv.store_id=' . Mage::app()->getStore()->getId() . ')',
                array()
            )
            ->joinLeft(
                array( 'mgdv' => $resource->getTableName( 'catalog/product_attribute_media_gallery_value' ) ),
                '(mg.value_id=mgdv.value_id AND mgdv.store_id=0)',
                array()
            )
            ->where( 'entity_id IN(?)', $ids );

        $mediaGalleryByProductId = array();

        $response = $conn->query($select);
        while ($gallery = $response->fetch()) {
            $id = $gallery['entity_id'];
            unset($gallery['entity_id']);

            if (!isset($mediaGalleryByProductId[$id])) {
                $mediaGalleryByProductId[$id] = [];
            }

            $mediaGalleryByProductId[$id][] = $gallery;
        }

        foreach ($productCollection as &$product) {
            $productId = $product->getEntityId();

            if (isset($mediaGalleryByProductId[$productId])) {
                $product->setData('media_gallery', array('images' => $mediaGalleryByProductId[$productId]));
            }
        }
    }
}