<?php

class Bunting_Personalisation_Adminhtml_BuntingController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        if (!$this->isModuleInstalled()) {
            return $this->renderModuleErrorLayout();
        }

        if ($this->isBuntingInstalled()) {
            return $this->_redirect('adminhtml/bunting/home');
        }

        $shopName = Mage::getStoreConfig('general/store_information/name');

        $this->loadLayout()
            ->_addContent(
                $this->getLayout()
                    ->createBlock('bunting_personalisation/adminhtml_install')
                    ->assign('shop_owner_email',Mage::getStoreConfig('trans_email/ident_general/email'))
                    ->assign('shop_name',$shopName)
                    ->assign('potential_subdomain',strtolower(preg_replace("/[^A-Za-z0-9]/", '', $shopName)))
                    ->assign('success_url',Mage::helper('adminhtml')->getUrl('adminhtml/bunting'))
                    ->setTemplate('bunting/install.phtml')
            )
            ->renderLayout();
    }

    public function homeAction()
    {
        if (!$this->isModuleInstalled()) {
            return $this->renderModuleErrorLayout();
        }

        if (!$this->isBuntingInstalled()) {
            $this->_redirect('adminhtml/bunting');
        }

        $bunting = $this->getBuntingCollection()->getFirstItem();

        $timestamp = time();
        $bunting_subdomain = $bunting->getBuntingSubdomain();
        $bunting_email = $bunting->getBuntingEmail();
        $account_key = $bunting_subdomain.$bunting_email.$timestamp;

        $message = '';
        if (isset($_SESSION['message'])) {
            $message = $_SESSION['message'];
            unset($_SESSION['message']);
        }

        $this->loadLayout()
            ->_addContent(
                $this->getLayout()
                    ->createBlock('bunting_personalisation/adminhtml_home')
                    ->assign('bunting_subdomain', $bunting_subdomain)
                    ->assign('bunting_region_id', $bunting->getServerRegionSubdomainId())
                    ->assign('timestamp', $timestamp)
                    ->assign('hash', hash_hmac('sha256', $account_key, '5dKc763_f}E5%s-'))
                    ->assign('password_api', $bunting->getPasswordApi())
                    ->assign('email_address',$bunting_email)
                    ->assign('message', $message)
                    ->setTemplate('bunting/home.phtml')
            )
            ->renderLayout();
    }

    public function loginAction()
    {
        $bunting_data = $this->submitToBunting('verify',[
            'email_address' => $this->getRequest()->getParam('verify_email_address'),
            'password' => $this->getRequest()->getParam('verify_password'),
            'subdomain' => $this->getRequest()->getParam('verify_bunting_subdomain')
        ]);

        $this->buntingResponse($bunting_data);
    }

    public function registerAction()
    {
        $full_locale = Mage::app()->getLocale()->getLocaleCode();
        list($language, $locale) = explode('_',$full_locale);

        $address = Mage::getStoreConfig('general/store_information/address');
        $address = $this->processAddress($address);

        $submit_data = [
            'billing' => 'automatic',
            'email_address' => $this->getRequest()->getParam('register_email_address'),
            'password' => $this->getRequest()->getParam('register_password'),
            'confirm_password' => $this->getRequest()->getParam('password_confirmation'),
            'subdomain' => $this->getRequest()->getParam('register_bunting_subdomain'),
            'name' => $this->getRequest()->getParam('company_name'),
            'forename' => $this->getRequest()->getParam('forename'),
            'surname' => $this->getRequest()->getParam('surname'),
            'telephone_number' => $this->getRequest()->getParam('telephone_number'),
            'promotional_code' => $this->getRequest()->getParam('promotional_code'),
            'timezone' => date_default_timezone_get(),
            'country' => $locale,
            'agency' => 'no'
        ];

        $submit_data = array_merge($submit_data, $address);
        $bunting_data = $this->submitToBunting('register',$submit_data);
        $this->buntingResponse($bunting_data);
    }

    public function unlinkAction() {
        foreach($this->getBuntingCollection() as $buntingObject) {
            /** @var Bunting_Personalisation_Model_Bunting $buntingObject */
            try {
                $buntingObject->delete();
            } catch(\Exception $e) {
                exit('Unable to unlink Bunting, please check your database connection');
            }
        }

        $this->_redirect('adminhtml/bunting/unlinked');
    }

    public function unlinkedAction() {
        $this->loadLayout()
            ->_addContent(
                $this->getLayout()
                    ->createBlock('bunting_personalisation/adminhtml_unlinked')
                    ->setTemplate('bunting/unlinked.phtml')
            )
            ->renderLayout();
    }

    public function optionsAction() {
        $imageStyle = (int) (isset($_POST['image_style']) ? $_POST['image_style'] : 0);
        $hideLowStock = (int) (isset($_POST['hide_low_stock']) ? $_POST['hide_low_stock'] : 0);
        $hideOutOfStock = (int) (isset($_POST['hide_out_of_stock']) ? $_POST['hide_out_of_stock'] : 0);
        Mage::getConfig()->saveConfig('bunting/settings/image_style', $imageStyle, 'default', 0);
        Mage::getConfig()->saveConfig('bunting/settings/hide_low_stock', $hideLowStock, 'default', 0);
        Mage::getConfig()->saveConfig('bunting/settings/hide_out_of_stock', $hideOutOfStock, 'default', 0);
        Mage::getSingleton('core/session')->addSuccess('Bunting plugin options saved');
        $this->_redirect('adminhtml/bunting/home');
    }

    /**
     * Render an error page if the module is not able to initialise
     */
    private function renderModuleErrorLayout() {
        $this->loadLayout()
            ->_addContent(
                $this->getLayout()
                    ->createBlock('bunting_personalisation/adminhtml_moduleerror')
                    ->setTemplate('bunting/module_error.phtml')
            )
            ->renderLayout();
    }

    private function processAddress($orig_address) {
        $comma_address = preg_replace('#\s+#',',',trim($orig_address));
        $address_array = explode(',', $comma_address);
        $address = [];
        if (!empty($address_array)) {
            $address['address_line_1'] = array_shift($address_array);
            $address['postcode'] = array_pop($address_array);
            $i = 2;
            foreach($address_array as $address_line) {
                $address['address_line_'.$i] = $address_line;
                if ($i < 5) {
                    $i++;
                }
                else {
                    break;
                }
            }
        }
        else {
            $address['address_line_1'] = $comma_address;
        }
        ksort($address);
        return $address;
    }

    private function submitToBunting($action, $params) {
        $timestamp = time();
        $storeData = $this->getStoresData($timestamp);

        $defaultParams = [
            'timestamp' => $timestamp,
            'hash' => hash_hmac('sha256', $timestamp, '5dKc763_f}E5%s-'),
            'plugin' => 'magento1',
            'create_website_monitor' => 'yes',
            'website_platform' => 'Magento 1',
            'multi' => $this->getStoresData($timestamp)
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.bunting.com/plugins/'.$action);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->http_build_query_for_curl($params + $defaultParams));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);

        if ($response === false) {
            $responseData = ['errors' => ['network' => 'Unable to connect to Bunting api']];
        } else {
            $responseData = json_decode($response, true);
        }

        if ($responseData['success']) {
            $responseData['email_address'] = $params['email_address'];
            $this->installBunting($responseData, $storeData);
        }
        return $responseData;
    }

    /**
     * Takes a response from the Bunting API and converts it to a valid frontend module response
     *
     * @param $buntingData
     * @return Zend_Controller_Response_Abstract
     */
    private function buntingResponse($buntingData) {
        $response = [];

        if ($buntingData['success']) {
            $response['message'] = $_SESSION['message'] = 'You can now login to Bunting.';
            return $this->sendJsonResponse($response);
        }

        $response['message'] = 'Please review the errors and try again.';

        if (!isset($buntingData['errors']) || !count($buntingData['errors'])) {
            $buntingData['errors'] = [];
            $response['message'] .= '<br><br>There was a problem connecting your shop to Bunting, please contact Bunting support.';
        }

        if (isset($buntingData['errors']['validation'])) {
            $response['message'] .= '<br><br>' . $buntingData['errors']['validation'];
        }

        $response['errors'] = $buntingData['errors'];
        return $this->sendJsonResponse($response);
    }

    /**
     * @param array $response
     * @return Zend_Controller_Response_Abstract
     */
    private function sendJsonResponse(array $response) {
        $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
        return $this->getResponse()->setBody(json_encode($response));
    }

    private function http_build_query_for_curl( $arrays, &$new = array(), $prefix = null ) {
        if ( is_object( $arrays ) ) {
            $arrays = get_object_vars( $arrays );
        }

        foreach ( $arrays as $key => $value ) {
            $k = isset( $prefix ) ? $prefix . '[' . $key . ']' : $key;
            if ( is_array( $value ) || is_object( $value )  ) {
                $this->http_build_query_for_curl( $value, $new, $k );
            } else {
                $new[$k] = $value;
            }
        }
        return $new;
    }

    private function installBunting($installData, $storeData) {
        $monitorIds = strpos($installData['website_monitor_id'], ',') !== false
            ? explode(',', $installData['website_monitor_id'])
            : [$installData['website_monitor_id']];

        $uniqueCodes = count($monitorIds) > 1 ? explode(',', $installData['unique_code']) : [$installData['unique_code']];

        $websiteMonitors = isset($installData['website_monitors'])
            ? count($monitorIds) > 1 ? explode(',', $installData['website_monitors']) : [$installData['website_monitors']]
            : [];

        foreach ($storeData as $id => $store) {
            $relativeId = $id;

            if (count($websiteMonitors)) {
                foreach ($websiteMonitors as $tempId => $websiteMonitor) {
                    $relativeId = ($store['domain_name'] . $storeData['directory']) == $websiteMonitor ? $tempId : $id;
                }
            }

            $bunting_model = Mage::getModel('bunting_personalisation/bunting');
            $bunting_model->setData([
                'bunting_email' => $installData['email_address'],
                'bunting_account_id' => $installData['account_id'],
                'bunting_website_monitor_id' => $monitorIds[$relativeId],
                'bunting_unique_code' => $uniqueCodes[$relativeId],
                'bunting_subdomain' => $installData['subdomain'],
                'feed_token' => $store['feed_token'],
                'password_api' => $installData['password_api'],
                'server_region_subdomain_id' => $installData['server_region_subdomain_id'],
                'store_id' => (int) $store['id']
            ]);
            $bunting_model->save();
        }
    }

    private function getBuntingCollection() {
        $bunting_model = Mage::getModel('bunting_personalisation/bunting');
        return $bunting_model->getCollection();
    }

    /**
     * Checks if the Bunting signup process is complete
     *
     * @return bool
     */
    private function isBuntingInstalled() {
        return $this->getBuntingCollection()->getSize();
    }

    /**
     * Checks first if the module has been correctly installed
     *
     * @return bool
     */
    private function isModuleInstalled() {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        $prefix = Mage::getConfig()->getTablePrefix();
        $tableName = isset($prefix) ? $prefix . 'bunting_personalisation_bunting' : 'bunting_personalisation_bunting';

        return $connection->isTableExists($connection->getTableName($tableName));
    }

    /**
     * Generate store URLs grouped togethed where they match, to be used for creating website monitors in Bunting
     *
     * @param string $timestamp
     * @return array
     */
    private function getStoresData($timestamp) {
        $storesData = [];

        foreach (Mage::app()->getWebsites() as $website) {
            foreach ($website->getGroups() as $group) {
                foreach ($group->getStores() as $store) {
                    $link = preg_replace(["/http[s]?:\/\//", "/\/index\.php\//"], '', $store->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK));

                    $storeData = [];
                    $storeData['ecommerce'] = 'yes';
                    $storeData['id'] = $store->getId();
                    $storeData['website_name'] = $store->getName();
                    $storeData['domain_name'] = preg_replace(["/http[s]?:\/\//", "/\/index\.php\//"], '', $store->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK));
                    $storeData['feed_token'] = md5($store->getId() . $store->getName() . $timestamp);
                    $storeData['product_feed-url'] = rtrim($link, '/') . '/bunting?feed_token=' . $storeData['feed_token'];
                    $storeData['product_feed-url_protocol'] = $store->isCurrentlySecure() ? 'https://' : 'http://';
                    $storeData['cart_url'] = Mage::getUrl('checkout/cart', ['_store' => $store->getId()]);

                    $link = explode("/", $link);
                    $storeData['domain_name'] = $link[0];
                    $link[0] = "";
                    $storeData['directory'] = rtrim(implode("/", $link), "/");

                    $full_locale = Mage::getStoreConfig('general/locale/code', $store->getId());
                    list($language, $locale) = explode('_',$full_locale);
                    $storeData['languages'] = array(strtoupper($language));

                    $currency_code = $store->getCurrentCurrencyCode();
                    $storeData['currencies'] = [[
                        'currency' => $currency_code,
                        'symbol' => html_entity_decode(Mage::app()->getLocale()->currency($currency_code)->getSymbol())
                    ]];

                    $storesData[] = $storeData;
                }
            }
        }

        return $storesData;
    }
}