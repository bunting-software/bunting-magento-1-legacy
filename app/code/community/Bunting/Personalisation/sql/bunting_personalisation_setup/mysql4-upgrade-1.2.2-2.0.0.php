<?php

$installer = $this;

$installer->startSetup();

$table = $installer->getConnection()
    ->addColumn(
        $installer->getTable('bunting_personalisation/bunting'),
        'store_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'nullable'  => false,
        ),
        'The store ID associated with this bunting installation'
    );

$installer->endSetup();