<?php

$installer = $this;

$installer->startSetup();

$table = $installer->getConnection()
    ->addColumn(
        $installer->getTable('bunting_personalisation/bunting'),
        'server_region_subdomain_id',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        null,
        array(
            'nullable'  => false,
        ),
        'Server Region Subdomain ID'
    );

$installer->endSetup();