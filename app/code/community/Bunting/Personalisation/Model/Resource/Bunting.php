<?php

class Bunting_Personalisation_Model_Resource_Bunting extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('bunting_personalisation/bunting', 'bunting_id');
    }
}