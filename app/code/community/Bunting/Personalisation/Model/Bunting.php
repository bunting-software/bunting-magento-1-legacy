<?php

class Bunting_Personalisation_Model_Bunting extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('bunting_personalisation/bunting');
    }

    public function getCategoryBreadcrumb($category, $category_string = '') {
        if ($category) {
            if ($category_string != '') {
                $category_string = '>'.$category_string;
            }
            $category_string = $category->getName().$category_string;
            $parent_category = $category->getParentCategory();
            if (!is_null($parent_category->getName())) {
                $category_string = $this->getCategoryBreadcrumb($parent_category, $category_string);
            }
        }
        else {
            $category_string = false;
        }
        return $category_string;
    }
}